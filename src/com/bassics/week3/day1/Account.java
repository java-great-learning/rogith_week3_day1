package com.bassics.week3.day1;

public class Account {
	private String accno;
	private double balance;
	public Account(String accno, double balance) {
		super();
		this.accno = accno;
		this.balance = balance;
	}
	
	@Override
	public String toString() {
		return "Account [accno=" + accno + ", balance=" + balance + "]";
	}

	public synchronized void withdraw(double amt) {
		System.out.println(Thread.currentThread().getName()+" in withraw");
		double bal = this.balance;
		if(bal > amt)
		{
			bal = bal - amt;
		}
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.balance = bal;
		System.out.println(Thread.currentThread().getName()+" withrawn ");
		
	}
	
}
