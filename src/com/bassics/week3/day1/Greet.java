package com.bassics.week3.day1;

public class Greet extends Thread {
	String name;
	public Greet()
	{
		super();
		this.name = name;
	}
	public synchronized void printGreeting(String guestName)
	{
		System.out.println("Welcome "+guestName);
		System.out.println("How are you doing ? "+guestName);
		System.out.println("Goodbye for now, see you soon "+guestName);
		
		try {
			Thread.sleep(500);
		}
		catch(InterruptedException e){
			e.printStackTrace();
		}
	}
	
	public void run()
	{
		String name = Thread.currentThread().getName();
		printGreeting(name);
	}
}
