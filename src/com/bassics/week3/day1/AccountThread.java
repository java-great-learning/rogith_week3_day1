package com.bassics.week3.day1;

public class AccountThread extends Thread {
	Account acc;
	double amount;
	
	public AccountThread(Account acc, String name, double amount) {
		super(name);
		this.acc = acc;
		this.amount = amount;
	}

	@Override
	public void run() {
		acc.withdraw(amount);
		System.out.println(acc);
	}
	public static void main(String[] args) throws InterruptedException {
		Account a1 = new Account("A1", 10000);
		Account a2 = new Account("A2", 15000);
		
		AccountThread t1 = new  AccountThread(a1, "T1", 2000);
		AccountThread t2 = new  AccountThread(a1, "T2", 3000);
		AccountThread t3 = new  AccountThread(a2, "T3", 5000);
		
		t1.start();
		t2.start();
		t3.start();
		t1.join();
		t2.join();
		System.out.println();
		System.out.println(a1);

	}


}
