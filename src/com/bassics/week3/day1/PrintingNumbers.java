package com.bassics.week3.day1;


public class PrintingNumbers extends Thread {
	
	public synchronized void run()
	{
		if(Thread.currentThread().getName().equals("even"))
		{
		for(int i=1;i<=20;i++)
		{
			if(i%2==0)
			{
				System.out.println(i);
			}
		}
		}
		
		if(Thread.currentThread().getName().equals("odd"))
		{
		for(int i=1;i<=20;i++)
		{
			if(i%2!=0)
			{
				System.out.println(i);
			}
		}
		}
	}
}
